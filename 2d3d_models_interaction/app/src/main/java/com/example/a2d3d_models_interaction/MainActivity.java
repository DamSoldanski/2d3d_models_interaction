package com.example.a2d3d_models_interaction;


import com.threed.jpct.Camera;
import com.threed.jpct.Config;
import com.threed.jpct.FrameBuffer;
import com.threed.jpct.Light;
import com.threed.jpct.Loader;
import com.threed.jpct.Logger;
import com.threed.jpct.Object3D;
import com.threed.jpct.Primitives;
import com.threed.jpct.RGBColor;
import com.threed.jpct.SimpleVector;
import com.threed.jpct.Texture;
import com.threed.jpct.TextureManager;
import com.threed.jpct.World;
import com.threed.jpct.util.BitmapHelper;
import com.threed.jpct.util.MemoryHelper;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.opengles.GL10;

public class MainActivity extends AppCompatActivity {

    // Used to handle pause and resume...
    private Object3D testModel = null;
    private static MainActivity master = null;
    private Context globalContext = null;
    private GLSurfaceView mGLView;
    private MyRenderer renderer = null;
    private FrameBuffer fb = null;
    private World world = null;
    private RGBColor back = new RGBColor(50, 50, 100);

    private float touchTurn = 0;
    private float touchTurnUp = 0;
    private float scale = 2;

    private float xpos = -1;
    private float ypos = -1;

    private Object3D cube = null;
    private int fps = 0;

    private Light sun = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (master != null) {
            copy(master);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // The callback can be enabled or disabled here or in handleOnBackPressed()


        Context context = this.getApplicationContext();
        Context globalContext = context;

        AssetManager assetManager = context.getAssets();
        List<Model> models = new ArrayList<Model>();
        loadAssets("models", assetManager, models);

        final List<String> modelsDescriptionsList = new ArrayList<String>();
        // add options for Spinner
        for (Model model : models) {
            modelsDescriptionsList.add(model.description);
        }

        Spinner chooseModelSpinner = (Spinner) findViewById(R.id.chooseModel);
        ArrayAdapter<String> adp1 = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, modelsDescriptionsList);
        adp1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        chooseModelSpinner.setAdapter(adp1);

        // remove me!!
        try {
            testModel = Object3D.mergeAll(Loader.load3DS(assetManager.open("models/balista.3ds"), scale));
        } catch (IOException e) {
            e.printStackTrace();
        }


        mGLView = new GLSurfaceView(getApplication());


        mGLView.setEGLConfigChooser(new GLSurfaceView.EGLConfigChooser() {
            public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display) {
                // Ensure that we get a 16bit framebuffer. Otherwise, we'll fall
                // back to Pixelflinger on some device (read: Samsung I7500)
                int[] attributes = new int[]{EGL10.EGL_DEPTH_SIZE, 16, EGL10.EGL_NONE};
                EGLConfig[] configs = new EGLConfig[1];
                int[] result = new int[1];
                egl.eglChooseConfig(display, attributes, configs, 1, result);
                return configs[0];
            }
        });

        renderer = new MyRenderer();
        mGLView.setRenderer(renderer);
        LinearLayout lL = findViewById(R.id.linearLayout);
        lL.addView(mGLView);

        chooseModelSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
                // TODO Auto-generated method stub
                String description = modelsDescriptionsList.get(position);

                for (Model model : models) {
                    if (description == model.description) {
                        // load selected model
                        try {
                            // trees9.3ds not working TODO remove
                            // loader has different methods to load different file types

                            Log.i("DDD6", "model loaded:" + model.path);
                            switch(model.extension) {
                                case "3ds":
                                    testModel = Object3D.mergeAll(Loader.load3DS(assetManager.open(model.path), scale));
                                    Log.i("DDD6", "model loaded:" + model.path);
                                    break;
                                case "obj":
                                    String[] fileNameSplit = model.path.split("\\.");
                                    String mtlFilePath = fileNameSplit[0];
                                    testModel = Object3D.mergeAll(Loader.loadOBJ(assetManager.open(model.path), assetManager.open(mtlFilePath+ ".mtl"), scale));
                                    break;
                            }
                            Toast.makeText(getBaseContext(), "Model '" + model.name + "." + model.extension + "' is selected", Toast.LENGTH_SHORT).show();

                        } catch (IOException e) {
                            Log.e("DDD6", "cube has problem");
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
    }


    @Override
    protected void onPause() {
        super.onPause();
        mGLView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGLView.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void copy(Object src) {
        try {
            Logger.log("Copying data from master Activity!");
            Field[] fs = src.getClass().getDeclaredFields();
            for (Field f : fs) {
                f.setAccessible(true);
                f.set(this, f.get(src));
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public boolean onTouchEvent(MotionEvent me) {

        if (me.getAction() == MotionEvent.ACTION_DOWN) {
            xpos = me.getX();
            ypos = me.getY();
            return true;
        }

        if (me.getAction() == MotionEvent.ACTION_UP) {
            xpos = -1;
            ypos = -1;
            touchTurn = 0;
            touchTurnUp = 0;
            return true;
        }

        if (me.getAction() == MotionEvent.ACTION_MOVE) {
            float xd = me.getX() - xpos;
            float yd = me.getY() - ypos;

            xpos = me.getX();
            ypos = me.getY();

            touchTurn = xd / -100f;
            touchTurnUp = yd / -100f;
            return true;
        }

        if (me.getAction() == MotionEvent.ACTION_POINTER_3_UP) {
            // ACTION_POINTER_3_UP
            // 3 fingers on screen tap
            // put 2 fingers on the screen and tap with the third finger
            scale = (float) (scale * 0.9);
            Log.i("DDD7", "ZOOM IN ACTION");
        } else if (me.getAction() == MotionEvent.ACTION_POINTER_2_UP) {
            // 2 fingers on screen tap
            // put 1 finger on the screen and tap with the second finger
            scale = (float) (scale * 1.1);
            Log.i("DDD7", "ZOOM OUT ACTION");
        }

        try {
            Thread.sleep(15);
        } catch (Exception e) {
            // No need for this...
        }

        return super.onTouchEvent(me);
    }

    protected boolean isFullscreenOpaque() {
        return true;
    }

    class MyRenderer implements GLSurfaceView.Renderer {

        private long time = System.currentTimeMillis();

        public MyRenderer() {
        }

        public void onSurfaceChanged(GL10 gl, int w, int h) {
            if (fb != null) {
                fb.dispose();
            }
            // this x, y changes only the model
            Log.e("DDD", gl.toString());
            Log.e("DDD", Integer.toString(w));
            Log.e("DDD", Integer.toString(h));
            fb = new FrameBuffer(gl, w, h);
            // if model 2d
            // draw it TODO
            if (master == null) {

                world = new World();
                world.setAmbientLight(100, 100, 100);

                sun = new Light(world);
                sun.setIntensity(250, 250, 250);

                // Create a texture out of the icon...:-)
                // deprecated + icon required ;/
                @SuppressLint("UseCompatLoadingForDrawables") Texture texture = new Texture(BitmapHelper.rescale(BitmapHelper.convert(getResources().getDrawable(R.drawable.ic_launcher_background, MainActivity.this.getTheme())), 64, 64));
                TextureManager.getInstance().addTexture("texture", texture);

//                cube = Primitives.getCube(10);
//                cube.calcTextureWrapSpherical();
//                cube.setTexture("texture");
//                cube.strip();
//                cube.build();
                // TODO use model in elegant way
                cube = testModel;
                world.addObject(cube);
                Camera cam = world.getCamera();
                cam.moveCamera(Camera.CAMERA_MOVEOUT, 50);
                cam.lookAt(cube.getTransformedCenter());

                SimpleVector sv = new SimpleVector();
                sv.set(cube.getTransformedCenter());
                sv.y -= 100;
                sv.z -= 100;
                sun.setPosition(sv);
                MemoryHelper.compact();

                if (master == null) {
                    Logger.log("Saving master Activity!");
                    master = MainActivity.this;
                }
            }
        }

        public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        }

        public void onDrawFrame(GL10 gl) {
            if (cube != testModel) {


                world.removeObject(cube);
                world.addObject(testModel);
                cube = testModel;
                Camera cam = world.getCamera();
                cam.moveCamera(Camera.CAMERA_MOVEOUT, 50);
                cam.lookAt(cube.getTransformedCenter());
                SimpleVector sv = new SimpleVector();
                sv.set(cube.getTransformedCenter());
                sv.y -= 100;
                sv.z -= 100;
                sun.setPosition(sv);
                MemoryHelper.compact();
            }

            if (touchTurn != 0) {
                cube.rotateY(touchTurn);
                touchTurn = 0;
            }

            if (touchTurnUp != 0) {
                cube.rotateX(touchTurnUp);
                touchTurnUp = 0;
            }

            cube.setScale(scale);
            fb.clear(back);
            world.renderScene(fb);
            world.draw(fb);
            fb.display();

            if (System.currentTimeMillis() - time >= 1000) {
                Logger.log(fps + "fps");
                fps = 0;
                time = System.currentTimeMillis();
            }
            fps++;
        }
    }

    private boolean loadAssets(String path, AssetManager assetManager, List<Model> models) {
        final List<String> supportedFilesTypes = Arrays.asList("3ds", "obj");

        String[] list;
        List<String> filesToLoad = new ArrayList<String>();
        try {
            list = getAssets().list(path);
            if (list.length > 0) {
                // This is a folder
                for (String file : list) {
                    String filePath = path + "/" + file;
                    if (!loadAssets(filePath, assetManager, models))
                        return false;
                    else {
                        Log.i("DDD2", filePath);
                        String[] filePathSplit = filePath.split("/");
                        String fileName = filePathSplit[filePathSplit.length - 1];
                        String[] fileNameSplit = fileName.split("\\.");
                        String extension = fileNameSplit[1];
                        if (supportedFilesTypes.contains(extension)) {
                            models.add(new Model(filePath, assetManager));
                            filesToLoad.add(filePath);
                        }
                    }
                }
            }
        } catch (IOException e) {
            return false;
        }
        return true;
    }
}