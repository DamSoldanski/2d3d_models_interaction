package com.example.a2d3d_models_interaction;

import android.content.res.AssetManager;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;


public class Model {
    String extension;
    String name;
    String path;
    String dir;
    String description;
    InputStream stream;


    public Model(String filePath, AssetManager assetManager) throws IOException {
        path = filePath;
        stream = assetManager.open(filePath);
        try {
            stream = assetManager.open(filePath);
            Log.i("DDD", stream.toString());
        } catch (IOException e) {
            Log.e("DDD", "error");
            e.printStackTrace();
        }
        String[] filePathSplit = filePath.split("/");
        String fileName = filePathSplit[filePathSplit.length - 1];
        String[] fileNameSplit = fileName.split("\\.");
        name = fileNameSplit[0];
        extension = fileNameSplit[1];
        description = "MODEL | name: " + name + ", extension: " + extension;
        Log.i("DDD", "ModelFile for file '" + path + "' created.");
    }
}